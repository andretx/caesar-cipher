#!/usr/bin/python
# -*- coding: utf-8 -*-

print("""
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	╔═╗┌─┐┬─┐┌─┐┌─┐  ╔═╗┌─┐┌─┐┌─┐┌─┐┬─┐  ╔═╗┬┌─┐┬ ┬┌─┐┬─┐
	╠╣ │ │├┬┘│  ├┤   ║  ├─┤├┤ └─┐├─┤├┬┘  ║  │├─┘├─┤├┤ ├┬┘
	╚  └─┘┴└─└─┘└─┘  ╚═╝┴ ┴└─┘└─┘┴ ┴┴└─  ╚═╝┴┴  ┴ ┴└─┘┴└─
					      
			[*] Author: andretx
			[*] https://bitbucket.org/andretx/
	
		Caesar Cipher in Python
	brute force, encryption and decryption functions.
	*BRUTEFORCE: y or n ( 'y' => yes 'n' => no )
	*ACTION: enc or dec ( 'enc' => encrypt 'dec' => decrypt )
	KEY: key chipher encrypt or decrypt
	*CIPHER: word to be encrypted or decrypted
	
	usage ASCII: www.asciitable.com
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
""")

TYPE = ''
KEY = ''

TEXT_CIPHER = 'CIPHER';

BRUTEFORCE = raw_input('BRUTEFORCE: ')
if (BRUTEFORCE != 'n') and (BRUTEFORCE != 'y'):
	print("Error invalid parameters *BRUTEFORCE -> %s" % (BRUTEFORCE))
	exit()

if (BRUTEFORCE == 'n'):
	TYPE = raw_input('ACTION: ')
	if (TYPE != 'enc') and (TYPE != 'dec'):
		print("Error invalid parameters *ACTION -> %s" % (TYPE))
		exit()

	if (TYPE == 'enc'):
		TEXT_CIPHER = 'TEXT'

	KEY = raw_input('KEY: ')
	KEY = int(KEY)
	if (not KEY) or (KEY > 26):
		print("Error invalid parameters KEY -> %s accepted 1 to 26" % (KEY))
		exit()

STRING_USER = raw_input(TEXT_CIPHER+': ')
STRING_USER = STRING_USER.upper()

# --
def cipher_enc_dec(string_user, key_cipher, type_user=''):

	import string

	RESULT = ''
	for lt in string_user:
		if (lt in string.ascii_letters):
			if (type_user != '') and (type_user == 'enc'):
				key = ord(lt) + key_cipher;
			else:
				key = ord(lt) - key_cipher;
			
			if (key <= 64):
				key = key + 26
			elif (key >= 90):
				key = key - 26

			RESULT += chr(key)
		else:
			RESULT += lt

	return RESULT
# --
	
if (BRUTEFORCE == 'y'):
	for key	in xrange(1,26):
		RESULT = cipher_enc_dec(STRING_USER,key)
		print("KEY: %s OUTPUT: %s" % (key,RESULT))
else:
	RESULT = cipher_enc_dec(STRING_USER,KEY,TYPE);
	print("OUTPUT: %s" % RESULT)

exit()
