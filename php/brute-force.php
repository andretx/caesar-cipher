#!/usr/bin/php
<?php
echo '          
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
	╔═╗┌─┐┬─┐┌─┐┌─┐  ╔═╗┌─┐┌─┐┌─┐┌─┐┬─┐  ╔═╗┬┌─┐┬ ┬┌─┐┬─┐
	╠╣ │ │├┬┘│  ├┤   ║  ├─┤├┤ └─┐├─┤├┬┘  ║  │├─┘├─┤├┤ ├┬┘
	╚  └─┘┴└─└─┘└─┘  ╚═╝┴ ┴└─┘└─┘┴ ┴┴└─  ╚═╝┴┴  ┴ ┴└─┘┴└─
			
			[*] Author: andretx
			[*] https://bitbucket.org/andretx/
	
		Caesar Cipher in PHP
	brute force, encryption and decryption functions.
	*BRUTEFORCE: y or n ( \'y\' => yes \'n\' => no )
	*ACTION: enc or dec ( \'enc\' => encrypt \'dec\' => decrypt )
	KEY: key chipher encrypt or decrypt
	*CIPHER: word to be encrypted or decrypted
	
	usage ASCII: www.asciitable.com
:::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::::
'.PHP_EOL;

$TYPE = '';
$KEY = '';

$TEXT_CIPHER = 'CIPHER';

fwrite(STDOUT, "BRUTEFORCE: ");
$BRUTEFORCE = trim(fgets(STDIN));
if( $BRUTEFORCE != 'n' && $BRUTEFORCE != 'y' ) { echo "Error invalid parameters *BRUTEFORCE -> ".$BRUTEFORCE.PHP_EOL; exit(0); }

if( $BRUTEFORCE == 'n' ) {
	fwrite(STDOUT, "ACTION: ");
	$TYPE = trim(fgets(STDIN));
	if( $TYPE != 'enc' && $TYPE != 'dec' ) { echo "Error invalid parameters *ACTION -> ".$TYPE.PHP_EOL; exit(0); }
	
	if( $TYPE == 'enc' ) { $TEXT_CIPHER = 'TEXT'; }
	
	fwrite(STDOUT, "KEY: ");
	$KEY = trim(fgets(STDIN));
	if( !$KEY || $KEY > 26 ) { echo "Error invalid parameters KEY -> ".$KEY." accepted 1 to 26".PHP_EOL; exit(0); }
}

fwrite(STDOUT, $TEXT_CIPHER.": ");
$STRING = trim(fgets(STDIN));

function cipher_enc_dec($string, $key_cipher, $type='') {

	$result = '';
	foreach( $string as $k => $value ) {
		if( ctype_alpha($value) ) {
			if( $type != '' && $type == 'enc' ) {
				$key = ord($value) + $key_cipher;
			} else {
				$key = ord($value) - $key_cipher;
			}

			if( $key <= 64 ) {
				$key = $key + 26;
			} elseif( $key >= 90 ) {
				$key = $key - 26;
			}

			$result .= chr($key);
		} else {
			$result .= $value;
		}
	  
	}

	return $result;
}

$STRING = trim($STRING);
$STRING = strtoupper($STRING);
$STRING = str_split($STRING);

if( $BRUTEFORCE == 'y' ) {
	$key_cipher = 1;
	while( $key_cipher <= 26 ) {
		$RESULT = cipher_enc_dec($STRING,$key_cipher);
		echo "KEY: ".$key_cipher." OUTPUT: ".$RESULT.PHP_EOL;
	$key_cipher++;
	}
} else {
	$RESULT = cipher_enc_dec($STRING,$KEY,$TYPE);
	echo "OUTPUT: ".$RESULT.PHP_EOL;
}

exit(0);
