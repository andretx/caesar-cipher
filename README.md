# BRUTE FORCE Caesar Cipher in PHP
Languages PHP:
* **PHP 7.3**

### Caesar Cipher
brute force, encryption and decryption functions.
- - -
* **brute-force.php**
```.php
php brute-force.php
```

# BRUTE FORCE Caesar Cipher in Python
Languages Python:
* **Python 2.7**
- - -
* **brute-force.py**
```.py
./brute-force.py
```

**#andretx**
